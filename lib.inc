%define STDIN 0
%define STDOUT 1
%define SYS_EXIT 60
%define SYS_WRITE 1
%define END_STRING 0xA


section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax
    .cycle:
       cmp byte[rdi + rax], 0
       jz .return
       inc rax
       jmp .cycle
    .return:
       ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rsi, rdi
    mov rdi, STDOUT
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
     push rdi
     mov rsi, rsp
     mov rax, SYS_WRITE
     mov rdi, STDOUT
     mov rdx, 1
     syscall
     pop rdi
     ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, END_STRING
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды
print_uint:
      xor rax, rax
      mov r8, rsp
      mov rax,rdi
      mov r10, 10
      push 0
      .loop:
     	 mov rdx, 0
     	 div r10; rax- hole part, rdx - float part
     	 add rdx, '0'
     	 dec rsp
     	 mov byte[rsp], dl
     	 cmp rax,0
     	 jz .end
      	jmp .loop
      .end:
      	mov rdi, rsp
      	call print_string
      	mov rsp, r8
      	ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov r10, rdi
    cmp r10, 0
    jnl .plus
    mov rdi, '-'
    call print_char
    neg r10
    mov rdi,r10
    .plus:
   	 call print_uint
   	 ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax,0
    .cycle:
  	 mov dl, [rdi + rax]
  	 cmp dl, [rsi +rax]
	 jne .lose
  	 inc rax
  	 cmp byte dl, 0
   	 jne .cycle
   .win: 
       mov rax,1
       ret
   .lose:
       mov rax,0
       ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
     xor rax, rax
     xor rdi, rdi
     push 0
     mov rdx, 1
     mov rsi,rsp
     syscall
     pop rax
     ret 
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:  
	push r12
	push rbx
	push rbp
	mov rbx, rdi
	mov rbp, rsi

	.read:
	    call read_char
	    cmp rax, 0x20
	    je .enter
            cmp rax, 0x9
            je .enter
            cmp rax, 0xA
            je .enter
            cmp rax,0
            je .end

            cmp r12, rbp
            jz .error

            mov [rbx +r12 ] , rax
            inc r12
            jmp .read
       .error:
            xor rax,rax
            mov rdx, r12
	    pop rbp
	    pop rbx
            pop r12
            ret

      .enter:
            cmp r12, 0
            jz .read

      .end:
            mov byte [rbx+r12], 0
            mov rdx, r12
            mov rax, rbx
	    pop rbp
            pop rbx
            pop r12
            ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
   xor rax, rax
   push rbx  
   xor rdx, rdx; long
   mov r11, 10; mul constant
  .A:
  	 xor rbx, rbx
  	 mov bl,[rdi + rdx]
	 cmp bl,  '0'
  	 js .E
  	 cmp bl,  '9'
  	 jna  .B
   	jmp .E
   .B:
  	 inc rdx
   	 sub bl, '0'
   	 push rdx
   	 mul r11 
   	 pop rdx
   	 add rax, rbx
  	 jmp .A
   
   .E:
   	pop rbx
   	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    push rbx
    xor rbx, rbx
    mov bl, [rdi+rdx]
    cmp bl, '+'
    jz .P
    cmp bl, '-'
    jz .M
    jmp .N

    .M:
   	 inc rdi
   	 call parse_uint
    	 neg rax
    	 inc rdx
   	 jmp .E
    .P:
    	inc rdi
   	 call parse_uint
    	inc rdx
    	jmp .E
    .N:
    	call parse_uint
    .E:
    	pop rbx
    	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax

    .write:
    	cmp rax,rdx
   	jae .error
    	mov r10, [rdi + rax]
    	mov [rsi+rax], r10 
    	inc rax
    	cmp r10, 0
    	jz .win
    	jmp .write

    .win:
    	ret

    .error:
    	mov rax, 0
   	ret
